
    const filmsUrl = "https://ajax.test-danit.com/api/swapi/films";
    const list = document.createElement("ul");
    document.body.prepend(list);
    function getData() {
        fetch(filmsUrl)
            .then(response => response.json())
            .then(films => {
                console.log(films);
                films.forEach(film => {
                    const li1 = document.createElement("li");
                    li1.innerHTML = `<b>Episode ${film["episodeId"]}. ${film["name"]}</b><br>${film["openingCrawl"]}`;
                    list.append(li1);
                    const listChar = document.createElement("ul");
                    li1.append(listChar);
                    film["characters"].forEach(characterUrl => {
                        fetch(characterUrl)
                            .then(response => response.json())
                            .then(character => {
                                console.log(character);
                                const li2 = document.createElement("li");
                                li2.innerText = character.name;
                                listChar.append(li2);
                            })
                            .catch(error => console.error('Error no information about the characters:', error));
                    });
                });
            })
            .catch(error => console.error('Error no information about the film:', error));
    }

    getData();








